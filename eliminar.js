

module.exports = (arregloUsuarios, usuarioAEliminar='')=>{
    const metodos= require('./paquetes')
    return new Promise ((resolve, reject)=>{
        metodos.buscarUsuario(arregloUsuarios, usuarioAEliminar).then(resultadoPromesaBuscar =>{
                arregloUsuarios.splice(resultadoPromesaBuscar.posicionUsuario, 1)
                resolve({
                    mensaje: `el usuario ${usuarioAEliminar}  fue eliminado`,
                    arregloConUsuarioEliminado: arregloUsuarios
                })
        })
        .catch(resultadoCatchBuscar=>{
            reject({
                mensaje: `${resultadoCatchBuscar.mensaje} por lo que no puede ser eliminado`,
                arregloConUsuarioEliminado: resultadoCatchBuscar.arregloUsuarios
            })
        })
    })
}