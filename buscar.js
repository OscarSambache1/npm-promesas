module.exports = (arregloUsuarios, usuarioABuscar='') => {
    const obtenerResultadoPromesBuscar = (resolve, reject) => {
        let posicionUsuario = -1;
        let arregloUsuariosEncontrados = arregloUsuarios.map((valor, indice) => {
            let seEncontroUsuario = valor.nombre.toLowerCase() === usuarioABuscar.toLowerCase()
            if (seEncontroUsuario) {
                posicionUsuario = indice
                return valor
            }
        }).filter(valor => {
            return valor !== undefined
        })

        if (arregloUsuariosEncontrados.length !== 0) {
            resolve({
                posicionUsuario: posicionUsuario,
                mensaje: `El usuario ${arregloUsuariosEncontrados[0].nombre} fue encontrado en el indice ${posicionUsuario}`,
                usuarioEncontrado: arregloUsuariosEncontrados[0],
                arregloUsuariosEncontrados
            })
        }
        else {
            reject({
                posicionUsuario: posicionUsuario,
                mensaje: `èl usuario ${usuarioABuscar} no fue encontrado`,
                usuarioEncontrado: arregloUsuariosEncontrados[0],
                arregloUsuariosEncontrados
            })
        }
    }
    return new Promise(obtenerResultadoPromesBuscar)
}
