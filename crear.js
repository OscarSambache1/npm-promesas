module.exports = (arregloUsuarios, usuarioACrear) => {
    const resultadoPromesaCrear = (resolve, reject) => {
        let usuarioCreado = new Object();
        usuarioCreado.nombre = usuarioACrear
        
        if(usuarioACrear && usuarioACrear != ''){
            arregloUsuarios.push(usuarioCreado)
            resolve({
                arregloUsuarios: arregloUsuarios,
                mensaje: `èl usuario ${usuarioCreado.nombre} fue creado`
            })
            
        }
        else{ 
            reject({
                arregloUsuarios: arregloUsuarios,
                mensaje: `el usuario esta indefinido por lo que no fue creado`
            })
        }
    }

    return new Promise(resultadoPromesaCrear)
}