const buscarCrearUsuario = require('./buscar-crear.js')
const eliminarUsuario = require('./eliminar')
const crearUsuario = require('./crear.js')
const buscarUsuario = require('./buscar.js')
const buscarTodos= require('./buscarPorExpresionRegular.js')
const datosPrueba= require('./datosPrueba')

module.exports = {
    buscarCrearUsuario,
    eliminarUsuario,
    crearUsuario,
    buscarUsuario,
    buscarTodos,
    datosPrueba
}