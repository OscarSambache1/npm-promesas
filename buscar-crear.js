

module.exports = (arregloUsuarios, usuarioBuscarCrear='') => {
    const metodos= require('./paquetes')
    return new Promise((resolve, reject) => {
        metodos.buscarUsuario(arregloUsuarios, usuarioBuscarCrear)
        .then(resultadoResolveBuscar => {
            resolve({
                mensaje: resultadoResolveBuscar.mensaje,
                arregloUsuarios: resultadoResolveBuscar.arregloUsuariosEncontrados
            })
        })
        .catch(resultadoRejectBuscar => {
            metodos.crearUsuario(arregloUsuarios, usuarioBuscarCrear)
            .then(resultadoResolveCrear => {
                reject({
                    mensaje:`${resultadoRejectBuscar.mensaje} por lo que ${resultadoResolveCrear.mensaje}`,
                    arregloUsuarios: resultadoResolveCrear.arregloUsuarios
                })
            })
            .catch(resultadoRejectCrear=>{
                reject({
                    mensaje: `${resultadoRejectBuscar.mensaje} por que  ${resultadoRejectCrear.mensaje}`,
                    arregloUsuarios: resultadoRejectCrear.arregloUsuariosEncontrados
                    
                    
                })
            })
        })
    })
}